#!/usr/bin/python
import pygame, sys, os, random, string, math

PROBLEM_COUNT = 20
NUM_LO = 1
NUM_HI = 10
# SUM_BOTTOM = 5
# SUM_TOP = 12
FPS = 30
TEST_DURATION = 120
COLUMNS = 3

TIME_BAR_FG = (40, 40, 20)
CURRENT_FG = (100, 200, 40)
CURRENT_ANSWER_FG = (200, 100, 40)
CURRENT_BG = (40, 40, 80)
NORMAL_FG = (200, 200, 40)
NORMAL_ANSWER_FG = (130, 100, 40)
NORMAL_BG = (20, 20, 20)
SCORE_FG = (200, 100, 40)

BUTTON_BG = (30, 30, 50)
BUTTON_HOVER_BG = (60, 60, 80)
BUTTON_PRESSED_BG = (80, 60, 60)
BUTTON_FG = (140, 140, 50)

CORRECT_FG = (20, 200, 20)
CORRECT_BG = (20, 60, 20)
WRONG_FG = (200, 40, 20)
WRONG_BG = (60, 40, 20)


SHIP_FG = (180, 20, 250)
SHIP_MARK_FG = (250, 250, 0)
DROPLET_FG = (180, 40, 20)

MIN_SCORE_TO_PLAY = 3
MIN_PLAY_SECONDS = 10
MAX_PLAY_SECONDS = 60
PLAY_STEP = 4
GEN_DROPLET_TICKS = int(FPS * 1.5)
DROPLET_YMAX = 200
DROP_GAME_BORDER_FG = (250, 250, 40)

DEFAULT_FONT_NAME = 'OpenSans-Regular.ttf'

A_plus_B_equals_WHAT = 0
A_plus_WHAT_equals_SUM = 1
WHAT_plus_B_equals_SUM = 2
WHAT_equals_A_plus_B = 3
SUM_equals_A_plus_WHAT = 4
SUM_equals_WHAT_plus_B = 5
PROBLEM_TYPES = 6


def color_code_to_rgb (color_code, intensity):
    r = (color_code >> 2) & 1
    g = (color_code >> 1) & 1
    b = color_code & 1
    return (r * intensity, g * intensity, b * intensity)

def wh_cmp (a, b):
    if a[0] == b[0] and a[1] == b[1]: return 0
    if a[0] > b[0] or a[1] > b[1]: return 1
    return -1

def wh_text_size (text, size, font_name, a = 6, b = 80):
    while a < b:
        c = (a + b) >> 1
        font = pygame.font.Font(font_name, c)
        text_surface = font.render(text, False, (0, 0, 0))
        cmp_result = wh_cmp(text_surface.get_size(), size)
        if cmp_result < 0: a = c + 1
        else: b = c - 1
    return c

def wh_text (text, size, color, font_name, a = 6, b = 80):
    font_size = wh_text_size(text, size, font_name, a, b)
    font = pygame.font.Font(font_name, font_size)
    return font.render(text, False, color)

class Mutuple (object):
    def __init__ (self, *a, **b):
        object.__init__(self)
        for i in (range(len(a))):
            setattr(self, self.fields[i], a[i])
        for k in b:
            setattr(self, k, b[k])

def mutuple (name, fields):
    return type(name, (Mutuple,), dict(fields=fields.split(), __dict__=fields.split()))

Problem = mutuple('Problem', 'mode a b r solved')
TestState = mutuple('TestState', 'problems pos tick ticks_total')

def build_test ():
    t = TestState([], 0, 0, FPS * TEST_DURATION)
    for i in range(PROBLEM_COUNT):
        # sum = random.randint(SUM_BOTTOM, SUM_TOP)
        # a = random.randint(1, sum - 1)
        # b = sum - a
        a = random.randint(NUM_LO, NUM_HI)
        b = random.randint(NUM_LO, NUM_HI)
        sum = a + b
        mode = random.randint(0, PROBLEM_TYPES - 1)
        t.problems.append(Problem(mode, a, b, 0, 0))
    return t

def solution (p):
    if p.mode == A_plus_B_equals_WHAT: return p.a + p.b
    if p.mode == A_plus_WHAT_equals_SUM: return p.b
    if p.mode == WHAT_plus_B_equals_SUM: return p.a
    if p.mode == WHAT_equals_A_plus_B: return p.a + p.b
    if p.mode == SUM_equals_A_plus_WHAT: return p.b
    if p.mode == SUM_equals_WHAT_plus_B: return p.a

def problem_text (p):
    if p.mode == A_plus_B_equals_WHAT: return '{} + {} = __'.format(p.a, p.b)
    if p.mode == A_plus_WHAT_equals_SUM: return '{} + __ = {}'.format(p.a, p.a + p.b)
    if p.mode == WHAT_plus_B_equals_SUM: return '__ + {} = {}'.format(p.b, p.a + p.b)
    if p.mode == WHAT_equals_A_plus_B: return '__ = {} + {}'.format(p.a, p.b)
    if p.mode == SUM_equals_A_plus_WHAT: return '{} = {} + __'.format(p.a + p.b, p.a)
    if p.mode == SUM_equals_WHAT_plus_B: return '{} = __ + {}'.format(p.a + p.b, p.b)

def update_answer (t, n):
    pb = t.problems[t.pos]
    if n < 0: pb.r = int(pb.r / 10)
    else: pb.r = pb.r * 10 + n
    pb.solved = 1 if solution(pb) == pb.r else 0

GUIState = mutuple('GUIState', 'surf w h font')

Droplet = mutuple('Droplet', 'xpos ypos radius speed')
DropGame = mutuple('DropGame', 'ppos pdelta droplets ticks_left ticks_total gen_droplet_ticks')

def gui_init (w, h):
    surf = pygame.display.set_mode((w, h), pygame.HWSURFACE | pygame.DOUBLEBUF | pygame.RESIZABLE)
    font_size = wh_text_size('99', (w, int(h * 9 / 10 / PROBLEM_COUNT)), DEFAULT_FONT_NAME)
    font = pygame.font.Font(DEFAULT_FONT_NAME, font_size)
    return GUIState(surf, w, h, font)

def rand_droplet ():
    return Droplet(random.randint(0, 100), 0, random.randint(1, 3), random.randint(1, 4))

def droplet_tick (d):
    d.ypos += d.speed
    return d.ypos < DROPLET_YMAX

def check_collision (ax, ay, ar, bx, by, br):
    return (ax - bx) * (ax - bx) + (ay - by) * (ay - by) < (ar + br) * (ar + br)

def main ():
    pygame.init() 
    pygame.display.set_caption('Math Test') 
    clock = pygame.time.Clock()

    f = open('scores.txt', 'a+')

    score = 0

    g = gui_init(1000, 750)

    t = build_test()

    done = False
    play = False
    allow_answer_updates = True

    drop_game = None
    an_hl = None
    while True:
        pygame.draw.rect(g.surf, NORMAL_BG, (0, 0, g.w, g.h))
        if not done:
            time_factor = float(t.tick) / t.ticks_total if t.tick < t.ticks_total else 1
            pygame.draw.rect(g.surf, (80 + 80 * time_factor, 160 - 80 * time_factor, 80), (10, 0, g.w / 20, g.h * time_factor))
        else:
            g.surf.blit(g.font.render('press F5 for new test', False, NORMAL_FG), (g.w / 2, 0))

        pb_x = g.w / 20 + 10
        pb_y = 0
        pb_h = g.h / PROBLEM_COUNT
        pb_w = g.w / 3
        an_x = g.w / 10 + 20 + g.w / 3 - g.w / 9
        for i in range(PROBLEM_COUNT):
            pb = t.problems[i]
            text = problem_text(pb)
            x = pb_x
            y = pb_y + i * pb_h
            bg = NORMAL_BG
            fg = NORMAL_FG
            if i == t.pos: 
                fg = CURRENT_FG
                bg = CURRENT_BG
            if done:
                bg = CORRECT_BG if pb.solved else WRONG_BG
                fg = CORRECT_FG if pb.solved else WRONG_FG
            pygame.draw.rect(g.surf, bg, (x, y, pb_w, pb_h - 1))
            text_surf = g.font.render(text, False, fg)
            g.surf.blit(text_surf, (pb_x + pb_x, pb_y + (i + 0.05) * pb_h))
            if pb.r is not None:
                rs = g.font.render('{}'.format(pb.r), False, CURRENT_ANSWER_FG if i == t.pos else NORMAL_ANSWER_FG)
                g.surf.blit(rs, (an_x, (i + 0.05) * pb_h))
        if done:
            g.surf.blit(wh_text('{}'.format(score), (g.w / 2, g.h), SCORE_FG, DEFAULT_FONT_NAME, 6, 200), (g.w / 2, 0))
        else:
            gx = g.w / 2
            gw = g.w - g.w / 20 - gx
            gy = 10
            gh = g.h - gy - 10
            bh = gh / 6
            bw = gw / 5

            pygame.draw.rect(g.surf, BUTTON_BG, (gx + 1, gy + bh / 4, gw - 2, bh / 2), 0)
            v = []
            v.append((gx + gw / 2, gy + bh / 3))
            v.append((gx + gw / 2 - bw / 4, gy + bh * 2 / 3))
            v.append((gx + gw / 2 + bw / 4, gy + bh * 2 / 3))
            pygame.draw.polygon(g.surf, BUTTON_FG, v, 0)

            pygame.draw.rect(g.surf, BUTTON_BG, (gx + 1, gy + 5 * bh + bh / 4, gw - 2, bh / 2), 0)
            v = []
            v.append((gx + gw / 2, gy + 5 * bh + bh * 2 / 3))
            v.append((gx + gw / 2 - bw / 4, gy + 5 * bh + bh * 1 / 3))
            v.append((gx + gw / 2 + bw / 4, gy + 5 * bh + bh * 1 / 3))
            pygame.draw.polygon(g.surf, BUTTON_FG, v, 0)

            for i in range(4):
                for j in range(5):
                    n = 1 + i * 5 + j
                    
                    pygame.draw.rect(g.surf, BUTTON_HOVER_BG if n == an_hl else BUTTON_BG, 
                            (gx + 1 + j * bw, gy + bh + 1 + i * bh, bw - 2, bh - 2), 0)
                    txt = wh_text('{}'.format(n), (bw - 4, bh - 4), BUTTON_FG, DEFAULT_FONT_NAME)
                    g.surf.blit(txt, (gx + j * bw + bw / 2 - txt.get_width() / 2, gy + bh + bh / 2 + i * bh - txt.get_height() / 2))


        if drop_game:
            time_factor = float(drop_game.ticks_left) / drop_game.ticks_total if drop_game.ticks_left >= 0 else 1
            pygame.draw.rect(g.surf, (20 + 20 * time_factor, 40 - 20 * time_factor, 20), (10, 0, g.w / 20, g.h * time_factor))

            gx = g.w / 2 + g.w / 20
            gw = g.w - g.w / 20 - gx
            gy = 10
            gh = g.h - gy - 10
            pr = int(g.w / 50)

            pygame.draw.rect(g.surf, DROP_GAME_BORDER_FG, (gx, gy + gh - 5, gw, 5), 1)

            cx = int(gx + drop_game.ppos * gw / 100)
            cy = int(gy + gh - pr)
            pygame.draw.circle(g.surf, SHIP_FG, (cx, cy), pr)
            pygame.draw.polygon(g.surf, SHIP_MARK_FG, ((cx, cy - pr * 3 / 4), (cx - pr / 2, cy + pr / 2), (cx + pr / 2, cy + pr / 2)), 0)
            for d in drop_game.droplets:
                pygame.draw.circle(g.surf, DROPLET_FG, (int(gx + d.xpos * gw / 100), int(gy + gh * d.ypos / DROPLET_YMAX)), pr * d.radius)


        pygame.display.flip()

        clock.tick(30)

        for e in pygame.event.get():
            if e.type == pygame.KEYDOWN:
                if e.key == pygame.K_ESCAPE: return
                elif e.key == pygame.K_F5: 
                    allow_answer_updates = True
                    done = False
                    t = build_test()
                elif e.key == pygame.K_F7: allow_answer_updates = True
                elif e.key == pygame.K_F8: t.tick += 300
                elif e.key == pygame.K_F9:
                    done = False
                    t.tick = t.ticks_total
                elif e.key in (pygame.K_RETURN, pygame.K_DOWN, pygame.K_j):
                    if t.pos < PROBLEM_COUNT - 1: t.pos += 1
                elif e.key in (pygame.K_UP, pygame.K_k):
                    if t.pos > 0: t.pos -= 1
                if allow_answer_updates:
                    if e.key == pygame.K_0: update_answer(t, 0)
                    elif e.key == pygame.K_1: update_answer(t, 1)
                    elif e.key == pygame.K_2: update_answer(t, 2)
                    elif e.key == pygame.K_3: update_answer(t, 3)
                    elif e.key == pygame.K_4: update_answer(t, 4)
                    elif e.key == pygame.K_5: update_answer(t, 5)
                    elif e.key == pygame.K_6: update_answer(t, 6)
                    elif e.key == pygame.K_7: update_answer(t, 7)
                    elif e.key == pygame.K_8: update_answer(t, 8)
                    elif e.key == pygame.K_9: update_answer(t, 9)
                    elif e.key == pygame.K_BACKSPACE: update_answer(t, -1)
            elif e.type == pygame.MOUSEMOTION and not done:
                x, y = e.pos
                if x >= gx and x - gx <= gw and y >= gy and y - gy <= gh:
                    i = int(math.floor((y - gy) / bh))
                    j = int(math.floor((x - gx) / bw))
                    an_hl = i * 5 + j - 5 + 1
            elif e.type == pygame.MOUSEBUTTONDOWN and not done:
                x, y = e.pos
                if x >= gx and x - gx <= gw and y >= gy and y - gy <= gh:
                    i = int(math.floor((y - gy) / bh))
                    j = int(math.floor((x - gx) / bw))
                    #print('{}x{}\n'.format(i, j))
                    if i == 0: 
                        if t.pos > 0: t.pos -= 1
                    elif i == 5:
                        if t.pos < PROBLEM_COUNT - 1: t.pos += 1
                    else:
                        n = i * 5 + j - 5 + 1
                        update_answer(t, -1)
                        update_answer(t, -1)
                        update_answer(t, -1)
                        update_answer(t, n)
                        if t.pos < PROBLEM_COUNT - 1: t.pos += 1
                #print('x={} y={} pb_x={} pb_y={} pb_w={} pb_h={}\n'.format(x, y, pb_x, pb_y, pb_w, pb_h))
                if x >= pb_x and x - pb_x < pb_w and y >= pb_y and y - pb_y < pb_h * PROBLEM_COUNT:
                    i = int(math.floor((y - pb_y) / pb_h))
                    t.pos = i
            elif e.type == pygame.VIDEORESIZE:
                g = gui_init(e.w, e.h)
            if drop_game:
                if e.type == pygame.KEYDOWN:
                    if e.key == pygame.K_LEFT: 
                        drop_game.pdelta = -PLAY_STEP
                    if e.key == pygame.K_RIGHT: drop_game.pdelta = +PLAY_STEP
                if e.type == pygame.KEYUP:
                    if drop_game.pdelta < 0 and e.key == pygame.K_LEFT: drop_game.pdelta = 0
                    if drop_game.pdelta > 0 and e.key == pygame.K_RIGHT: drop_game.pdelta = 0

        t.tick += 1
        if not done and t.tick >= t.ticks_total:
            done = True
            allow_answer_updates = False
            score = sum(pb.solved for pb in t.problems)
            f.write('{}\n'.format(score))
            if score >= MIN_SCORE_TO_PLAY:
                ticks = FPS * (MIN_PLAY_SECONDS + (MAX_PLAY_SECONDS - MIN_PLAY_SECONDS) * score / PROBLEM_COUNT)
                drop_game = DropGame(50, 0, [], ticks, ticks, GEN_DROPLET_TICKS)

        if drop_game:
            dl = []
            for d in drop_game.droplets:
                if droplet_tick(d): dl.append(d)
            drop_game.droplets = dl
            drop_game.ppos += drop_game.pdelta
            if drop_game.ppos < 0: drop_game.ppos = 0
            if drop_game.ppos > 100: drop_game.ppos = 100
            if drop_game.ticks_left % drop_game.gen_droplet_ticks == 0:
                drop_game.droplets.append(rand_droplet())
            game_ended = False

            gx = g.w / 2 + g.w / 20
            gw = g.w - g.w / 20 - gx
            gy = 10
            gh = g.h - gy - 10
            pr = int(g.w / 50)

            for d in drop_game.droplets:
                if check_collision(int(gx + drop_game.ppos * gw / 100), int(gy + gh - pr), int(g.w / 50), 
                        int(gx + d.xpos * gw / 100), int(gy + gh * d.ypos / DROPLET_YMAX), pr * d.radius):
                    game_ended = True
            drop_game.ticks_left -= 1
            if drop_game.ticks_left < 0: game_ended = True
            if game_ended:
                drop_game = None

main()

