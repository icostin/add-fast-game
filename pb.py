import random
PROBLEM_COUNT=40
SUM_BOTTOM = 8
SUM_TOP = 20

A_plus_B_equals_WHAT = 0
A_plus_WHAT_equals_SUM = 1
WHAT_plus_B_equals_SUM = 2
WHAT_equals_A_plus_B = 3
SUM_equals_A_plus_WHAT = 4
SUM_equals_WHAT_plus_B = 5
PROBLEM_TYPES = 6

class Mutuple (object):
    def __init__ (self, *a, **b):
        object.__init__(self)
        for i in (range(len(a))):
            setattr(self, self.fields[i], a[i])
        for k in b:
            setattr(self, k, b[k])

def mutuple (name, fields):
    return type(name, (Mutuple,), dict(fields=fields.split(), __dict__=fields.split()))

Problem = mutuple('Problem', 'mode a b r solved')
TestState = mutuple('TestState', 'problems pos tick ticks_total')

def build_test ():
    t = TestState([], 0, 0, 1)
    for i in range(PROBLEM_COUNT):
        sum = random.randint(SUM_BOTTOM, SUM_TOP)
        a = random.randint(1, sum - 1)
        b = sum - a
        mode = random.randint(0, PROBLEM_TYPES - 1)
        t.problems.append(Problem(mode, a, b, 0, 0))
    return t

def solution (p):
    if p.mode == A_plus_B_equals_WHAT: return p.a + p.b
    if p.mode == A_plus_WHAT_equals_SUM: return p.b
    if p.mode == WHAT_plus_B_equals_SUM: return p.a
    if p.mode == WHAT_equals_A_plus_B: return p.a + p.b
    if p.mode == SUM_equals_A_plus_WHAT: return p.b
    if p.mode == SUM_equals_WHAT_plus_B: return p.a

def problem_text (p):
    if p.mode == A_plus_B_equals_WHAT: return '{} + {} = __'.format(p.a, p.b)
    if p.mode == A_plus_WHAT_equals_SUM: return '{} + __ = {}'.format(p.a, p.a + p.b)
    if p.mode == WHAT_plus_B_equals_SUM: return '__ + {} = {}'.format(p.b, p.a + p.b)
    if p.mode == WHAT_equals_A_plus_B: return '__ = {} + {}'.format(p.a, p.b)
    if p.mode == SUM_equals_A_plus_WHAT: return '{} = {} + __'.format(p.a + p.b, p.a)
    if p.mode == SUM_equals_WHAT_plus_B: return '{} = __ + {}'.format(p.a + p.b, p.b)

def main ():
    t = build_test()
    for pb in t.problems:
        print('{}'.format(problem_text(pb)))

main()
        
